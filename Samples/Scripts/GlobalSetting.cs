using UnityEngine;

namespace UniVent.GlobalSetting
{
    public class GlobalSetting : MonoBehaviour
    {
        public static int PrintFrequency = 1;

        public int mirror = 1;

        private void FixedUpdate()
        {
            PrintFrequency = mirror;
        }
    }
}
