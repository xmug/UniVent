﻿using UnityEngine;
using UniVent;

namespace WHAT
{
    public class ShopEvent : INetworkEvent
    {
        public int shopItemId;
    }
    
    public class ShopEvent2 : INetworkEvent
    {
        public int shopItemId;
    }
    public class Sender : MonoBehaviour
    {
        [SerializeField]
        private int shopItemIdOutSide;

        private int m_ShopItemId;

        public int ShopItemId
        {
            get => m_ShopItemId;
            set
            {
                if(m_ShopItemId != value)
                {
                    m_ShopItemId = value;
                    OnShopItemIdChanged(m_ShopItemId);
                }
            }
        }

        private void FixedUpdate()
        {
            ShopItemId = shopItemIdOutSide;
        }

        private void OnShopItemIdChanged(int id)
        {
            // 频繁事件
            new ShopEvent(){shopItemId = id}.InvokeEvent();
            new ShopEvent2(){shopItemId = id}.InvokeEvent();
        }
    }
}