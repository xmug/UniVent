﻿using UnityEngine;

namespace UniVent
{
    public class OldReceiver : MonoBehaviour, OldSender.IOnValueChangedCallBack
    {
        public int valueReceived;
        
        private void Start()
        {
            OldSender.instance.AddOnValueChangedCallBack(this);
        }

        public void OnValueChangedCallBack(int oldValue, int newValue)
        {
            valueReceived = newValue;
            // Debug.Log($"[OldReceiver] <OnValueChangedCallBack> OldValue:{oldValue}, NewValue:{newValue}");
        }
    }
}