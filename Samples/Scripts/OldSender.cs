﻿using System.Collections.Generic;
using UnityEngine;

namespace UniVent
{
    public class OldSender : MonoBehaviour
    {
        public static OldSender instance;

        private void Awake()
        {
            instance = this;
        }

        public interface IOnValueChangedCallBack
        {
            public void OnValueChangedCallBack(int oldValue, int newValue);
        }

        private readonly List<IOnValueChangedCallBack> _callBacks = new List<IOnValueChangedCallBack>();

        public void AddOnValueChangedCallBack(IOnValueChangedCallBack callBack)
        {
            _callBacks.Add(callBack);
        }
        
        public void RemoveOnValueChangedCallBack(IOnValueChangedCallBack callBack)
        {
            _callBacks.Remove(callBack);
        }
        
        private int m_ValueToListen = 0;

        public int ValueToListen
        {
            get => m_ValueToListen;
            set
            {
                if (value != m_ValueToListen)
                {
                    foreach (var callBack in _callBacks)
                    {
                        callBack.OnValueChangedCallBack(m_ValueToListen, value);
                    }
                    m_ValueToListen = value;
                }
            }
        }

        public int valueSetter;

        // 0.002
        private void FixedUpdate()
        {
            for (int i = 0; i < GlobalSetting.GlobalSetting.PrintFrequency; i++)
            {
                ValueToListen = ValueToListen > 0 ? 0 : 1;
                valueSetter = ValueToListen;
            }
        }
    }
}