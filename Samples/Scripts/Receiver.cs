using UnityEngine;
using WHAT;

namespace UniVent
{
    public class Receiver : MonoBehaviour, INetworkEventReceiver<ShopEvent>, INetworkEventReceiver<ShopEvent2>
    {
        private void Awake()
        {
            this.RegisterEvent<ShopEvent>();
            this.RegisterEvent<ShopEvent2>();
        }

        void INetworkEventReceiver<ShopEvent>.Callback(ShopEvent eventParam)
        {
            Debug.Log($"[Receiver] <ShopEvent> 1111, json: {((INetworkEvent)eventParam).ToJsonStr()}");
        }

        void INetworkEventReceiver<ShopEvent2>.Callback(ShopEvent2 eventParam)
        {
            Debug.Log($"[Receiver] <ShopEvent> 2222, json: {((INetworkEvent)eventParam).ToJsonStr()}");
        }
    }
}