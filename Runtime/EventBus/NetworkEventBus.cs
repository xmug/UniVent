using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UniVent
{
    public static class NetworkEventInvoker<TEvent> where TEvent : INetworkEvent
    {
        private static Lazy<string> cache = new(() => typeof(TEvent).ToString(), true);
        public static string CachedTEventIdentifier => cache.Value;
    }
    
    public interface INetworkEvent
    {
        public string ToJsonStr()
            => JsonConvert.SerializeObject(this, Formatting.None,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                });

        public static TEvent FromJsonStr<TEvent>(string str) where TEvent : INetworkEvent
        {
            try
            {
                return JsonConvert.DeserializeObject<TEvent>(str,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                    });
            }
            catch (Exception e)
            {
                throw new Exception($"[MiniGameEvent] ParseJsonStr Failed, {str}\n" +
                                    $"Exception: {e}");
            }
        }
    }

    public interface INetworkEventReceiver<in TEvent> where TEvent : INetworkEvent
    {
        protected internal void Callback(TEvent eventParam);
    }
    
    public static class NetworkEventBus
    {
        public interface IRawNetworkEventReceiver
        {
            protected internal void Callback(string str);
        }

        private class NetworkEventHandler<TEvent> : IRawNetworkEventReceiver where TEvent : INetworkEvent
        {
            private INetworkEventReceiver<TEvent> m_BoundReceiver;

            public NetworkEventHandler(INetworkEventReceiver<TEvent> boundReceiver)
            {
                this.m_BoundReceiver = boundReceiver ?? throw new NullReferenceException();
            }

            void IRawNetworkEventReceiver.Callback(string str)
            {
                m_BoundReceiver.Callback(INetworkEvent.FromJsonStr<TEvent>(str));
            }
        }

        public static Dictionary<string, List<IRawNetworkEventReceiver>> dict = new();

        public static void RegisterEvent<TEvent>(this INetworkEventReceiver<TEvent> eventReceiver) where TEvent : INetworkEvent
        {
            var networkHandler = new NetworkEventHandler<TEvent>(eventReceiver);
            var typeFullName = NetworkEventInvoker<TEvent>.CachedTEventIdentifier;
            if (!dict.TryGetValue(typeFullName, out var rawNetworkEventReceivers))
            {
                rawNetworkEventReceivers = new List<IRawNetworkEventReceiver>();
                dict.Add(typeFullName, rawNetworkEventReceivers);
            }

            rawNetworkEventReceivers.Add(networkHandler);
        }

        public static void InvokeEvent<TEvent>(this TEvent networkEvent)
            where TEvent : INetworkEvent
        {
            NetworkTransSend(NetworkEventInvoker<TEvent>.CachedTEventIdentifier, networkEvent.ToJsonStr());
        }

        public static void NetworkTransSend(string typeFullName, string str)
        {
            NetworkTransReceive(typeFullName, str);
        }

        public static void NetworkTransReceive(string typeFullName, string str)
        {
            foreach (IRawNetworkEventReceiver IRawNetworkEventReceiver in dict[typeFullName])
            {
                IRawNetworkEventReceiver.Callback(str);
            }
        }
    }
}