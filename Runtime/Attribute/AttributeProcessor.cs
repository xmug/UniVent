using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace UniVent
{
    [InitializeOnLoad]
    public class AttributeProcessor
    {
        static AttributeProcessor()
        {
            Type[] allTypes = System.AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes())
                .ToArray();
            (Type type, MethodInfo[] methods, IEnumerable<Attribute>[] methodToAttributes)[] allTuples = allTypes
                .SelectMany<Type, (Type, MethodInfo[], IEnumerable<Attribute>[])>(type =>
                {
                    var allMethods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
                    return new[]
                    {
                        (type, allMethods,
                            allMethods.Select(method => method.GetCustomAttributes(typeof(EventCallback))).ToArray())
                    };
                }).ToArray();
            
            for (int i = 0; i < allTuples.Length; i++)
            {
                var tuple = allTuples[i];
                for (int j = 0; j < tuple.methods.Length; j++)
                {
                    (Type type, MethodInfo method, IEnumerable<Attribute> attributes) = (tuple.type,
                        tuple.methods[j], tuple.methodToAttributes[j]);
                    foreach (Attribute customAttribute in attributes)
                    {
                        GenerateCode(type, method, customAttribute as EventCallback);
                        Debug.Log($"[AttributeProcessor] NameSpace: {type.Namespace}, FullName: {type.FullName} Method: {method.Name}, CustomAttribute: {customAttribute}");
                    }
                }
            }
        }

        static void GenerateCode(Type type, MethodInfo method, EventCallback attribute)
        {
            
        }
    }
}