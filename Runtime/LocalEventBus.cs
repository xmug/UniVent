using System;
using System.Diagnostics.CodeAnalysis;

namespace UniVent
{
    public interface ILocalEvent : IEvent
    {
        /// <summary>
        /// 运行时赋值，请勿修改<br/>
        /// 声明事件时，请 显式实现 以屏蔽事件参数
        /// </summary>
        protected internal int EventIndex { get; set; }

        internal static int GetEventIndex<TEvent>(TEvent localEvent) where TEvent : struct, ILocalEvent => localEvent.EventIndex;
    }

    public interface ILocalEventSender<TEvent> : IEventSender where TEvent : struct, IEvent
    {
        /// <summary>
        /// 本地事件的全局唯一标识，与事件发送者绑定<br/>
        /// 请 显式实现 以防止意外调用<br/>
        /// </summary>
        protected internal TEvent EventIdentification { get; set; }

        internal static TEvent GetEventIdentification(ILocalEventSender<TEvent> eventSender) =>
            eventSender.EventIdentification;
    }

    public interface ILocalEventReceiver<in TEvent> : IEventReceiver<TEvent> where TEvent : struct, IEvent
    {

    }

    public static partial class EventBus
    {
        public static void RegisterEvent<TEvent>(this ILocalEventReceiver<TEvent> eventReceiver,
            [NotNull] ILocalEventSender<TEvent> eventSender) where TEvent : struct, ILocalEvent
        {
            // 如果此时EventReceiver尚未初始化/实例化，不添加
            if (eventReceiver == null)
            {
                throw new ArgumentNullException(nameof(eventReceiver));
            }

            int eventIndex = eventSender.EventIdentification.EventIndex;
            if (eventIndex == 0)
            {
                eventSender.EventIdentification = new TEvent() { EventIndex = GetNextEventIndex() };
                eventIndex = eventSender.EventIdentification.EventIndex;
            }
            else
            {
                eventIndex = TrySetEventIndex<TEvent>(eventIndex);
            }

            AddCallback(eventIndex, eventReceiver, EventCallBacks);
        }


        public static void RegisterEvent<TEventReceiver, TEvent>(this ref TEventReceiver eventReceiver,
            [NotNull] ILocalEventSender<TEvent> eventSender) where TEventReceiver : struct, ILocalEventReceiver<TEvent>
            where TEvent : struct, ILocalEvent
        {
            int eventIndex = eventSender.EventIdentification.EventIndex;
            if (eventIndex == 0)
            {
                eventSender.EventIdentification = new TEvent() { EventIndex = GetNextEventIndex() };
                eventIndex = eventSender.EventIdentification.EventIndex;
            }
            else
            {
                eventIndex = TrySetEventIndex<TEvent>(eventIndex);
            }

            AddCallback(eventIndex, eventReceiver, EventCallBacks);
        }


        public static void UnRegisterEvent<TEvent>([NotNull] this ILocalEventReceiver<TEvent> eventReceiver,
            [NotNull] ILocalEventSender<TEvent> eventSender) where TEvent : struct, ILocalEvent
        {
            int eventIndex = eventSender.EventIdentification.EventIndex;
            if (eventIndex == 0)
            {
                return;
            }

            RemoveCallback(eventIndex, eventReceiver);
        }

        public static void UnRegisterEvent<TEventReceiver, TEvent>(this ref TEventReceiver eventReceiver,
            [NotNull] ILocalEventSender<TEvent> eventSender) where TEventReceiver : struct, ILocalEventReceiver<TEvent>
            where TEvent : struct, ILocalEvent
        {
            int eventIndex = eventSender.EventIdentification.EventIndex;
            if (eventIndex == 0)
            {
                return;
            }

            RemoveCallback(eventIndex, eventReceiver);
        }

        public static void InvokeEvent<TEventSender, TEvent>([NotNull] this TEventSender eventSender,
            TEvent eventParam) where TEventSender : ILocalEventSender<TEvent> where TEvent : struct, ILocalEvent
        {
            int eventIndex = eventSender.EventIdentification.EventIndex;
            if (eventIndex == 0)
            {
                return;
            }

            InternalInvokeEvent(eventParam, eventIndex, EventCallBacks);
        }
    }
}