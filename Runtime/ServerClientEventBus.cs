using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace UniVent
{
    public interface IServerClientEvent : IEvent
    {
        
    }
    
    public interface IServerEvent : IServerClientEvent
    {
        
    }
    
    public interface IClientEvent : IServerClientEvent
    {
        
    }
    public static partial class EventBus
    {
        #region ServerClientEventCount

        private static int m_ServerEventIdCount = 0;
        private static int m_ClientEventIdCount = 0;
        private static int ServerEventIdCount
        {
            get
            {
                if (m_ServerEventIdCount == 0)
                {
                    m_ServerEventIdCount = Enum.GetNames(typeof(ClientEventId)).Length;
                }

                return m_ServerEventIdCount;
            }
        }
        private static int ClientEventIdCount
        {
            get
            {
                if (m_ClientEventIdCount == 0)
                {
                    m_ClientEventIdCount = Enum.GetNames(typeof(ClientEventId)).Length;
                }

                return m_ClientEventIdCount;
            }
        }

        #endregion

        private static readonly IEventCallBack[] ServerEventCallBacks = new IEventCallBack[ServerEventIdCount];

        private static readonly IEventCallBack[] ClientEventCallBacks = new IEventCallBack[ClientEventIdCount];

        public static void RegisterEvent<TEvent>(this ServerEventId serverEventId,
            [NotNull] IEventReceiver<TEvent> eventReceiver) where TEvent : struct, IServerEvent
        {
            int eventIndex = (int)serverEventId;
            
            if (ServerEventCallBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.CallBacks.Add(eventReceiver);
            }
            else
            {
                EventCallBack<TEvent> newEventCallBack = new EventCallBack<TEvent>()
                {
                    CallBacks = new List<IEventReceiver<TEvent>>(256) { eventReceiver }
                };
                ServerEventCallBacks[eventIndex] = newEventCallBack;
            }
        }
        
        public static void UnRegisterEvent<TEvent>(this ServerEventId serverEventId,
            [NotNull] IEventReceiver<TEvent> eventReceiver) where TEvent : struct, IServerEvent
        {
            int eventIndex = (int)serverEventId;
            
            if (ServerEventCallBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.CallBacks.Remove(eventReceiver);
            }
        }
        
        public static void RegisterEvent<TEvent>(this ClientEventId clientEventId,
            [NotNull] IEventReceiver<TEvent> eventReceiver) where TEvent : struct, IClientEvent
        {
            int eventIndex = (int)clientEventId;
            
            if (ClientEventCallBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.CallBacks.Add(eventReceiver);
            }
            else
            {
                EventCallBack<TEvent> newEventCallBack = new EventCallBack<TEvent>()
                {
                    CallBacks = new List<IEventReceiver<TEvent>>(256) { eventReceiver }
                };
                ClientEventCallBacks[eventIndex] = newEventCallBack;
            }
        }
        
        public static void UnRegisterEvent<TEvent>(this ClientEventId clientEventId,
            [NotNull] IEventReceiver<TEvent> eventReceiver) where TEvent : struct, IClientEvent
        {
            int eventIndex = (int)clientEventId;
            
            if (ClientEventCallBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.CallBacks.Remove(eventReceiver);
            }
        }
        
        public static void InvokeEvent<TEvent>(this ServerEventId serverEventId, TEvent eventParam) where TEvent : struct, IServerEvent
        {
            int eventIndex = (int)serverEventId;

            InternalInvokeEvent(eventParam, eventIndex, ServerEventCallBacks);
        }
        
        public static void InvokeEvent<TEvent>(this ClientEventId clientEventId, TEvent eventParam) where TEvent : struct, IClientEvent
        {
            int eventIndex = (int)clientEventId;

            InternalInvokeEvent(eventParam, eventIndex, ClientEventCallBacks);
        }
    }
}