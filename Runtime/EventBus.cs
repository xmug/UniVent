using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace UniVent
{
    public interface IEvent
    {
    }

    public interface IEventSender
    {
    }

    public interface IEventReceiver<in TEvent> where TEvent : IEvent
    {
        protected internal void CallBack(TEvent eventParam);
    }

    public static partial class EventBus
    {
        private interface IEventCallBack
        {
        }

        private class EventCallBack<T> : IEventCallBack where T : IEvent
        {
            public int callBackCount => CallBacks.Count;
            public List<IEventReceiver<T>> CallBacks = null;
        }

        private const int kInvalidIndexOffset = 1;
        private static readonly List<bool> EventIndexArray = new List<bool>(new bool[1024]);

        private static readonly List<IEventCallBack>
            EventCallBacks = new List<IEventCallBack>(new IEventCallBack[1024]);

        private static int GetNextEventIndex()
        {
            int rt = 0;

            // +1 是为了排除0，使得0作为没有初始化的事件的标志
            for (int i = kInvalidIndexOffset; i < EventIndexArray.Count; i++)
            {
                rt = EventIndexArray[i] == false ? i : rt;
                if (rt != 0)
                {
                    EventIndexArray[i] = true;
                    return rt;
                }
            }

            EventIndexArray.Add(false);
            EventCallBacks.Add(null);
            rt = EventIndexArray.Count - 1;

            return rt;
        }

        private static int TrySetEventIndex<TEvent>(int index)
            where TEvent : struct, IEvent
        {
            // 如果原先分配的超出了当前的上限，则对应的Callback必然已经清空，分配新的
            if (index >= EventIndexArray.Count)
            {
                return GetNextEventIndex();
            }

            // 如果此位置已经被其他人申请到了
            if (EventCallBacks[index] is not EventCallBack<TEvent> eventCallBack)
            {
                return GetNextEventIndex();
            }

            // 如果分配的位置的Callback已经清空，则申请一个新的, 保证 EventArray 的最小占用
            if (EventIndexArray[index] == false)
            {
                return GetNextEventIndex();
            }

            // 如果还有Callback，则继续添加
            return index;
        }


        private static void AddCallback<TEvent>(int eventIndex, [NotNull] IEventReceiver<TEvent> eventReceiver,
            IList<IEventCallBack> callBacks)
            where TEvent : struct, IEvent
        {
            if (callBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.CallBacks.Add(eventReceiver);
            }
            else
            {
                EventCallBack<TEvent> newEventCallBack = new EventCallBack<TEvent>()
                {
                    CallBacks = new List<IEventReceiver<TEvent>>(256) { eventReceiver }
                };
                callBacks[eventIndex] = newEventCallBack;
            }
        }

        private static void RemoveCallback<TEvent>(int eventIndex, [NotNull] IEventReceiver<TEvent> eventReceiver)
            where TEvent : struct, ILocalEvent
        {
            if (EventIndexArray[eventIndex] == false) return;

            if (EventCallBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.CallBacks.Remove(eventReceiver);

                // 解除占用
                if (eventCallBack.callBackCount <= 0)
                {
                    EventIndexArray[eventIndex] = false;
                }
            }
        }

        private static void InternalInvokeEvent<TEvent>(TEvent eventParam, int eventIndex, IList<IEventCallBack> callBacks)
            where TEvent : struct, IEvent
        {
            if (callBacks[eventIndex] is EventCallBack<TEvent> eventCallBack)
            {
                eventCallBack.Invoke(eventParam);
            }
        }

        private static void Invoke<TEvent>(this EventCallBack<TEvent> callBacks, TEvent eventParam)
            where TEvent : struct, IEvent
        {
            int c = callBacks.callBackCount;
            List<IEventReceiver<TEvent>> interfaceCallBacks = callBacks.CallBacks;
            for (int i = 0; i < c; i++)
            {
                interfaceCallBacks[i].CallBack(eventParam);
            }
        }
    }
}